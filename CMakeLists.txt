cmake_minimum_required(VERSION 3.10)
project(deps)

# Configure path to download build and install dependecies
set(SUIL_EP_PREFIX "${CMAKE_BINARY_DIR}/3rdParty" CACHE STRING "The root directory used by ExternalAdd_Project")

include(ExternalProject)

set_directory_properties(PROPERTIES EP_PREFIX ${SUIL_EP_PREFIX})
set(EP_INSTALL_DIR ${SUIL_EP_PREFIX}/os)

ExternalProject_Add(libmill
        PREFIX ${SUIL_EP_PREFIX}/libmill
        GIT_REPOSITORY https://gitlab.com/sw-devel/thirdparty/libmill.git
        CMAKE_ARGS "-DCMAKE_INSTALL_PREFIX=${EP_INSTALL_DIR}")

ExternalProject_Add(catch
        PREFIX ${SUIL_EP_PREFIX}/catch
        GIT_REPOSITORY https://github.com/catchorg/Catch2.git
        GIT_TAG "v2.13.1"
        CMAKE_ARGS "-DCMAKE_INSTALL_PREFIX=${EP_INSTALL_DIR};-DCATCH_BUILD_EXAMPLES=OFF;-DCATCH_BUILD_TESTING=OFF;-DCATCH_INSTALL_DOCS=OFF;-DCATCH_INSTALL_HELPERS=OFF")

ExternalProject_Add(iod
        PREFIX ${SUIL_EP_PREFIX}/iod
        GIT_REPOSITORY https://gitlab.com/sw-devel/thirdparty/iod.git
        CMAKE_ARGS "-DCMAKE_INSTALL_PREFIX=${EP_INSTALL_DIR}")

include_directories(${EP_INSTALL_DIR}/include)
link_directories(${EP_INSTALL_DIR}/lib)
set(CMAKE_MODULE_PATH
        ${CMAKE_MODULE_PATH} ${EP_INSTALL_DIR}/share/cmake/Modules)

add_custom_target(deps
        DEPENDS libmill catch iod)
